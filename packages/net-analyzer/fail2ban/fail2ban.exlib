# Copyright 2012-2013 Romain Lalaut <romain@quarkstudio.fr>
# Copyright 2016-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'fail2ban-0.8.6.ebuild' from Gentoo, which is:
#       Copyright 1999-2012 Gentoo Foundation

require github systemd-service
require setup-py [ python_opts="[readline][sqlite]" import=setuptools has_bin=true has_lib=true multibuild=false ]

export_exlib_phases src_install

SUMMARY="An intrusion prevention framework"
DESCRIPTION="
Fail2ban scans log files (e.g. /var/log/apache/error_log) and bans IPs
that show the malicious signs -- too many password failures, seeking
for exploits, etc. Generally Fail2Ban then used to update firewall rules
to reject the IP addresses for a specified amount of time, although any
arbitrary other action (e.g. sending an email, or ejecting CD-ROM tray)
could also be configured. Out of the box Fail2Ban comes with filters
for various services (apache, curier, ssh, etc).
"
HOMEPAGE="http://www.${PN}.org/"

BUGS_TO="romain@quarkstudio.fr philantrop@exherbo.org"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/wiki/index.php/Manual"

LICENCES="GPL-2"
SLOT="0"

DEPENDENCIES="
    run:
        dev-python/dnspython[python_abis:*(-)?]
    suggestion:
        app-admin/gamin[>=0.0.21][python_abis:*(-)?] [[
            description = [ allow gamin as backend used to get files modifications ]
        ]]
        dev-python/pyinotify[>=0.8.3][python_abis:*(-)?] [[
            description = [ allow pyinotify as backend used to get files modifications ]
        ]]
        dev-python/python-systemd[python_abis:*(-)?] [[
            description = [ support the systemd journal as a logging backend ]
        ]]
"

# The tests are hanging indefinitely.
# Lots of sandbox violations as well.
# (last checked: 0.9.4)
RESTRICT="test"

fail2ban_src_install() {
    setup-py_src_install

    doman man/*.1

    install_systemd_files
    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins ${PN}.conf <<EOF
d /run/fail2ban 0755 root root
EOF

    keepdir /etc/fail2ban/fail2ban.d
    keepdir /etc/fail2ban/jail.d
    keepdir /var/lib/${PN}

    edo mv "${IMAGE}"/usr/share/doc/fail2ban/* "${IMAGE}"/usr/share/doc/${PNVR}
    edo rmdir "${IMAGE}"/usr/share/doc/fail2ban

    edo rm -r "${IMAGE}"/run
}

