# Copyright 2017 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

DOWNLOAD_ID="2423"

require cmake [ api=2 ] systemd-service

SUMMARY="OpenVAS scanner"
HOMEPAGE="http://www.openvas.org"
DOWNLOADS="http://wald.intevation.org/frs/download.php/${DOWNLOAD_ID}/${PNV}.tar.gz"

LICENCES="GPL-2 LGPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.32]
        dev-libs/libgcrypt
        net-analyzer/openvas-libraries[>=9.0.0]
    run:
        dev-db/redis
        net-scanner/nmap [[ note = [ The predefined scan configurations needs nmap as a port scanner ] ]]
    test:
        dev-util/cppcheck
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DDATADIR:PATH=/usr/share
    -DLIBDIR:PATH=/usr/$(exhost --target)/lib
    -DLOCALSTATEDIR:PATH=/var
    -DOPENVAS_RUN_DIR:PATH=/run
    -DSBINDIR:PATH=/usr/$(exhost --target)/bin
    -DSYSCONFDIR:PATH=/etc
)

src_install() {
    cmake_src_install

    install_systemd_files

    insinto /etc/openvas
    hereins openvassd.conf << EOF
kb_location = /tmp/redis.sock
EOF

    keepdir /var/{cache,lib,log}/openvas
    keepdir /var/lib/openvas/plugins
}

