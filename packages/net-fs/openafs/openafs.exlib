# Copyright 2009 Bo Ørsted Andresen
# Distributed under the terms of the GNU General Public License v2

export_exlib_phases pkg_pretend src_install

SUMMARY="OpenAFS is an open source implementation of the Andrew distributed file system (AFS)"
DESCRIPTION="AFS is a distributed filesystem allowing cross-platform sharing of files among multiple
computers. Facilities are provided for access control, authentication, backup and administrative
management."
HOMEPAGE="http://www.openafs.org/"

LICENCES="IPL-1.0 APSL-2.0 IPL-1.0 Sun-RPC"
SLOT="0"
MYOPTIONS="
    supergroups [[ description = [ Enables support of nested groups in the ptserver. ] ]]
    platform: amd64 x86
"

DEPENDENCIES="
    build:
        sys-devel/flex
    build+run:
        virtual/kerberos
    suggestion:
        sys-auth/pam-afs-session
        sys-auth/pam-krb5
"

openafs_pkg_pretend() {
    if option !platform:amd64 && option !platform:x86; then
        die "Platform not supported, you need to update the exheres to handle your platform"
    fi
}

openafs_src_install() {
    default

    dodir /usr/src/libafs
    edo "${WORK}"/src/config/make_libafs_tree.pl \
        -p "${WORK}" \
        -os LINUX \
        -sn ${sysname} \
        -t "${IMAGE}"/usr/src/libafs

    insinto /usr/share/doc/${PNVR}
    hereins libafs <<'EOF'
To compile and install the libafs kernel module you need to tell it where the configured kernel
sources are:

# cd /usr/src/libafs
# ./configure --with-linux-kernel-headers=${KERNEL_BUILD_DIR} --with-linux-kernel-build=${KERNEL_BUILD_DIR}
# make

Then you need to manually copy the kernel module:

# mkdir -p /lib/modules/${KERNEL}/kernel/fs/openafs
# cp src/libafs/MODLOAD-*/libafs.ko /lib/modules/${KERNEL}/kernel/fs/openafs

To let modprobe libafs work (which is needed by the init script) you may need to:

# depmod -a
EOF

    dodoc src/SOURCE-MAP

    install_systemd_files

    insinto /etc/${PN}
    doins "${FETCHEDDIR}"/CellServDB
    hereins ThisCell <<EOF
openafs.org
EOF
    hereins cacheinfo <<EOF
/afs:/var/cache/openafs:96000
EOF

    hereconfd afsserver.conf <<EOF

# See bosserver(8) man page
BOSSERVER_ARGS=""
EOF

    hereconfd afsclient.conf <<EOF
# See afsd(8) man page
AFSD_ARGS=""
EOF

    keepdir /var/cache/openafs

    edo rmdir "${IMAGE}"/usr/${LIBDIR}/${PN}
}

