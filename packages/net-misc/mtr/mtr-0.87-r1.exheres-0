# Copyright 2009 Mike Kelly
# Copyright 2013 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2

# configure was generated with the gtk+ macros missing (afaict)
# last checked: 0.87
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="an improved traceroute / ping"
DESCRIPTION="
mtr combines the functionality of the 'traceroute' and 'ping' programs
in a single network diagnostic tool.

As mtr starts, it investigates the network connection between the host
mtr runs on and a user-specified destination host. After it determines
the address of each network hop between the machines, it sends a
sequence ICMP ECHO requests to each one to determine the quality of the
link to each machine. As it does this, it prints running statistics
about each machine.
"
HOMEPAGE="http://www.bitwizard.nl/${PN}/"
DOWNLOADS="ftp://ftp.bitwizard.nl/${PN}/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gtk"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/glib:2
        gtk? ( x11-libs/gtk+:2[>=2.6.0] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=( --enable-ipv6 )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( gtk )

pkg_postinst() {
    # make install sets 'cap_net_raw+ep' but we can't rely on paludis preserving that
    # (because e.g. binaries)
    nonfatal edo setcap cap_net_raw+ep  "${ROOT}"/usr/$(exhost --target)/bin/${PN} \
        ||   edo chmod u+s              "${ROOT}"/usr/$(exhost --target)/bin/${PN}
}

