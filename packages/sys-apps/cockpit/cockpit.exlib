# Copyright 2014-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=cockpit-project release=${PV} suffix=tar.xz ] \
    pam \
    systemd-service \
    freedesktop-desktop

export_exlib_phases src_install pkg_postinst pkg_postrm

SUMMARY="A sysadmin login session in a web browser"
DESCRIPTION="
Cockpit is an interactive server admin interface. It is easy to use and very light weight. Cockpit
interacts directly with the operating system from a real Linux session in a browser.

Cockpit makes Linux discoverable, allowing sysadmins to easily perform tasks such as starting
containers, storage administration, network configuration, inspecting logs and so on.

Jumping between the terminal and the web tool is no problem. A service started via Cockpit can be
stopped via the terminal. Likewise, if an error occurs in the terminal, it can be seen in the
Cockpit journal interface.

On the Cockpit dashboard, you can easily add other machines with Cockpit installed that are
accessible via SSH.
"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    doc
    (
        cockpit_components:
            docker         [[ description = [ Component for managing Docker containers ] ]]
            machines       [[ description = [ Component for virtual machines management supported by libvirt ] ]]
            networkmanager [[ description = [ Component for network management using NetworkManager ] ]]
            pcp            [[ description = [ Cockpit support for reading PCP metrics and loading PCP archives ] ]]
            storage        [[ description = [ Component for storage management supported by udisks:2 ] ]]
    )
"

# test-webservice fails when cockpit is already installed
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-libs/libxslt
        dev-util/intltool
        sys-devel/gettext
        virtual/pkg-config[>=0.9.0]
        doc? (
            app-text/docbook-xsl-stylesheets
            app-text/xmlto
        )
    build+run:
        user/cockpit-ws
        group/cockpit-ws
        app-crypt/krb5
        core/json-glib[>=0.14.0]
        dev-libs/glib:2[>=2.37.4]
        gnome-desktop/libgudev[>=164]
        net-libs/libssh[>=0.6.0]
        sys-apps/systemd[>=187] [[ note = [ for logind ] ]]
        sys-auth/polkit:1[>=0.105]
        sys-libs/pam
        cockpit_components:pcp? ( monitor/pcp )
    run:
        dev-libs/glib-networking[gnutls(+)][ssl(+)]
        cockpit_components:docker? (
            group/docker
            app-virtualization/docker
        )
        cockpit_components:machines? ( virtualization-lib/libvirt )
        cockpit_components:networkmanager? ( net-apps/NetworkManager[>=0.9.7.0] )
        cockpit_components:storage? ( sys-apps/udisks:2[>=2.6.4] )
    recommendation:
        app-crypt/sscg[>=2.3.0] [[ description = [ Use SSCG for generating self-signed certificates ] ]]
    suggestion:
        dev-libs/libpwquality [[ description = [ Adds support to display the password strength when creating users ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --localstatedir=/var
    --enable-ssh
    --with-cockpit-user=cockpit-ws
    --with-networkmanager-needs-root=no
    --with-pamdir=$(getpam_mod_dir)
    --with-storaged-iscsi-sessions
    --with-systemdunitdir=${SYSTEMDSYSTEMUNITDIR}
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    doc
    "cockpit_components:pcp pcp"
)

cockpit_src_install() {
    default

    # certificates
    keepdir /etc/cockpit/ws-certs.d

    # machines
    keepdir /etc/cockpit/machines.d

    # configuration
    keepdir /var/lib/cockpit

    # pam configuration
    pamd_mimic_system cockpit auth auth account session

    # remove Fedora firewalld stuff
    edo rm -rf "${IMAGE}"/usr/$(exhost --target)/lib/firewalld

    # remove branding for other distributions
    edo rm -rf "${IMAGE}"/usr/share/cockpit/branding/{centos,debian,fedora,kubernetes,registry,rhel,ubuntu}

    # remove debug data for stuff in /usr/share
    # https://github.com/cockpit-project/cockpit/issues/3288
    edo rm -rf "${IMAGE}"/usr/$(exhost --target)/src

    # cockpit packages
    # remove Applications component (uses PackageKit and AppStream)
    edo rm -rf "${IMAGE}"/usr/share/cockpit/apps
    # remove Kernel Dump component (while we have kexec-tools it requires a running kdump.service, see Fedora sources)
    edo rm -rf "${IMAGE}"/usr/share/cockpit/kdump
    # remove Kubernetes Cluster component
    edo rm -rf "${IMAGE}"/usr/share/cockpit/kubernetes
    # remove OSTree component
    edo rm -rf "${IMAGE}"/usr/share/cockpit/ostree
    # remove Virtual Machines Management (oVirt)
    edo rm -rf "${IMAGE}"/usr/share/cockpit/ovirt
    # remove PackageKit component (PackageKit lacks a Paludis backend)
    edo rm -rf "${IMAGE}"/usr/share/cockpit/packagekit
    # remove Playground component
    edo rm -rf "${IMAGE}"/usr/share/cockpit/playground
    # remove realmd component
    edo rm -rf "${IMAGE}"/usr/share/cockpit/realmd
    # remove Red Hat Enterprise Linux subscription component
    edo rm -rf "${IMAGE}"/usr/share/cockpit/subscriptions
    # remove SELinux component
    edo rm -rf "${IMAGE}"/usr/share/cockpit/selinux
    # remove Diagnostic reports component
    edo rm -rf "${IMAGE}"/usr/share/cockpit/sosreport
    # remove Tuned component
    edo rm -rf "${IMAGE}"/usr/share/cockpit/tuned

    option cockpit_components:docker || edo rm -rf "${IMAGE}"/usr/share/cockpit/docker
    option cockpit_components:machines || edo rm -rf "${IMAGE}"/usr/share/cockpit/machines
    option cockpit_components:networkmanager || edo rm -rf "${IMAGE}"/usr/share/cockpit/networkmanager
    option cockpit_components:pcp || edo rm -rf "${IMAGE}"/usr/share/cockpit/pcp
    option cockpit_components:storage || edo rm -rf "${IMAGE}"/usr/share/cockpit/storaged
}

cockpit_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
}

cockpit_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
}

